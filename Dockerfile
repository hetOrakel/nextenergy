FROM debian:12

LABEL Maintainer="Henrie Cuijpers <henrie.cuijpers@gmail.com>"
LABEL Description="Prototype mijn.nextenergy.nl api"

ENV TZ=Europe/Amsterdam
SHELL ["/bin/bash", "-c"]

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update &&  apt-get install -y \
  cron \
  python3 \
  python3-requests
RUN apt-get clean

WORKDIR /src
COPY src/. .

CMD [ "python3", "-u", "/src/main.py"]
