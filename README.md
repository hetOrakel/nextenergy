# nextenergy API POC

## Proof of concept voor NextEnergy.nl API
Dit is geen production code, het is enkel een mogelijk voorbeeld voor het
gebruik van de API op mijn.nextenergy.nl om daarmee gedetailleerde persoonlijk
info op te kunnen halen.

## Description
Python-script (in docker) die de next-energy api's gebruikt voor het ophalen van
marktprijzen en je eigen verbruik info. Exact gelijk zoals het in de app
gebeurt.

## Benodigdheden - Installatie - Gebruik
Voor het gebruik is enkel docker en docker-compose nodig. Alles is zo opgezet
dat het in een docker container draait. Alles wat nodig is voor de werking wordt
automatisch opgehaald binnen de docker container.

- kloon de git repo
- maak een kopie van .env.example met als naam .env en vul je data in
- start de container: `docker-compose up`

De eerste keer zal alle benodigde info opgehaald worden. Een volgende keer wordt
gebruik gemaakt van lokale cache. Docker regelt dit allemaal automatisch voor
je. De docker zal daarna het verloop tonen en na afloop van het script stopt de
docker. Met een parameter kun je regelen dat de docker blijft draaien en 1x per
24 uur de info bijwerkt.

Bij het ophalen van de data zijn bewust random vertragingen geïntroduceerd om
"hameren" op de servers te voorkomen.

## Usage report
Er is een "report_usage" toegevoegd die 1x per 24 hr (of beter elke keer dat het
script data ophaalt, wat in "keeprunning" mode 1x per 24hr is) een ping doet.
Hoewel eea vrij gedeeld en aanpasbaar is, vraag ik vriendelijk om dit te laten
staan. Op deze manier kunnen er later bv grafiekjes gemaakt worden van bv het
aantal unieke gebruikers.

## .env file
In de .env file kunnen wat instellingen gezet worden:

    { "userName":"your-user-name",
      "password":"your-password",
      "keepRunning": false,
      "catchUp": 0,
      "catchUpOffset": 0
    }

 * userName / password
    - spreekt voor zich hoop ik
 * keepRunning
    - false = 1x alles ophalen en stoppen
    - true = na het ophalen wordt er gewacht to ca 17:00 (de volgende dag) en
      dan wordt er weer een ophaalactie gestart. Dit blijft zich herhalen tot de
      docker (geforceerd) gestopt wordt.
 * catchUp
    - indien groter dan 0: aantal dagen om in te halen. Er wordt in dat geval
      voor het aantal dagen gelijk aan de waarde van catchUp een verzoek
      verzonden. Daarna stopt de verwerking ongeacht de waarde van keepRunning.
    - indien 0: bij elke run wordt enkel de vorige dag en het maandtotaal
      opgehaald; zie ook keepRunning
 * catchUpOffset
    - startdag voor ophalen data. Werkt enkel in combinatie met catchUp.
      1 = gisteren
      2 = eergisteren enz enz
 * logToTerminal
    - true = logging gaat naar de terminal
    - false = logging gaat naar ./data/nextenergyapi.log
