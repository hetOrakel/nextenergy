import json
import logging
import random
import requests, pickle
import sys
import time
import urllib.parse
import uuid
from datetime import datetime,timezone,timedelta,date
from pathlib import Path
from hashlib import sha256

settings = {}
infostore = {}
settingsfile = "/srv/data/.env"
infostorefile = "/srv/data/infostore"
logfile = "/srv/data/nextenergyapi.log"
usageData = []
usageDataMnd = []
sleepEnabled = True
catchUp = 0
catchUpOffset = 0
usageDataFileDag = '/srv/data/usage-data-p-day.json'
usageDataFileMnd = '/srv/data/usage-data-p-mnd.json'
basehost = 'https://mijn.nextenergy.nl'
headers = { 'Content-type': 'application/json'
          , 'Accept': 'application/json'
          , 'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 16_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 OutSystemsApp v.3.82'
          , 'Accept-Language':'en-GB,en;q=0.9'
          , 'Accept-Encoding': 'gzip, deflate, br'
          }
distribution      = { 1: "p_mnd"       , 2: "p_dag"     , 3: "p_uur"            }
usageUtilityType  = { 0: "eur"         , 1: "gas_m3"    , 2: "ele_kWh"          }
usageCostLevel    = { 1: "Inkoopprijs" , 2: "AllIn"     , 3: "InkoopprijsPlus"  }
apiVersion        = { 'usageData'   : '0IF7YPWHNS8DfkTjYGHvGA'
                    , 'marketPrices': 'GK6FkxOydmjZpUSC8a+fuw'
                    , 'login'       : '6RXNTWQg6FlgienWK8L57A'
                    , 'registerUser': 'peZ7YvhVQDO_8uFsnaFmng'
                    , 'contractData': 'OWZ27jy7wRowZbWGBlpucg'
                    }


def startup():
  global settings,infostore,usageData,usageDataMnd,catchUp,catchUpOffset
  # -- loading settings
  if Path(settingsfile).is_file():
    print("Load settings...")
    settings = json.load(open(settingsfile, 'r'))
  else:
    print(".env not found, see readme")
    raise ValueError(".env not found, see readme")
  if 'logToTerminal' not in settings:
    print("logToTerminal not found in .env, compare your version with the example")
    raise ValueError("logToTerminal not found in .env, compare your version with the example")
  if settings['logToTerminal'] :
    print( "All messages and logging goes to terminal...")
    logging.basicConfig( encoding='utf-8', level=logging.INFO, format='%(asctime)s %(message)s')
  else :
    print( "All messages and logging goes to logfile...")
    logging.basicConfig(filename=logfile, encoding='utf-8', level=logging.INFO, format='%(asctime)s %(message)s')
  logging.info(f"------- Starting ----- {datetime.now().strftime('%Y-%m-%d %H:%M')} --")
  if 'keepRunning' not in settings:
    logging.error("keepRunning not found in .env, compare your version with the example")
    raise ValueError("keepRunning not found in .env, compare your version with the example")
  if 'catchUp' not in settings:
    logging.error("catchUp not found in .env, compare your version with the example")
    raise ValueError("catchUp not found in .env, compare your version with the example")
  catchUp = settings['catchUp']
  if 'catchUpOffset' not in settings:
    logging.error("catchUpOffset not found in .env, compare your version with the example")
    raise ValueError("catchUpOffset not found in .env, compare your version with the example")
  catchUpOffset = settings['catchUpOffset']
  #print(json.dumps(settings))
  # -- creating / loading infostore
  if Path(infostorefile).is_file():
    logging.info("Load saved info...")
    infostore = json.load(open(infostorefile, 'r'))
  else:
    logging.info("infostore file not found, creating one...")
    infostore["Created"] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f' )[:-3]+"Z"
    with open(infostorefile, 'w', encoding="utf-8") as f:
      json.dump(infostore,f)
  #print(json.dumps(infostore))
  # -- creating / loading Device ID
  if "DeviceId" in infostore:
    logging.info ( f"Device ID exists, nice...")
  else:
    infostore["DeviceId"] =  str(uuid.uuid4()).upper()
    logging.info ( f"Device ID not found, creating one...")
    with open(infostorefile, 'w', encoding="utf-8") as f:
      json.dump(infostore,f)
  if "OneSignalAppId" in infostore:
    logging.info ( f"OneSignalAppId ID exists, nice...")
  else:
    infostore["OneSignalAppId"] =  str(uuid.uuid4()).lower()
    logging.info ( f"OneSignalAppId ID not found, creating one...")
    with open(infostorefile, 'w', encoding="utf-8") as f:
      json.dump(infostore,f)
  if "OneSignalUserId" in infostore:
    logging.info ( f"OneSignalUserId ID exists, nice...")
  else:
    infostore["OneSignalUserId"] =  str(uuid.uuid4()).lower()
    logging.info ( f"OneSignalUserId ID not found, creating one...")
    with open(infostorefile, 'w', encoding="utf-8") as f:
      json.dump(infostore,f)
  if "OneSignalPushToken" in infostore:
    logging.info ( f"OneSignalPushToken ID exists, nice...")
  else:
    infostore["OneSignalPushToken"] =  sha256( str(uuid.uuid4()).lower().encode('utf-8') ).hexdigest()
    logging.info ( f"OneSignalPushToken ID not found, creating one...")
    with open(infostorefile, 'w', encoding="utf-8") as f:
      json.dump(infostore,f)
  # -- creating / loading usagedatfile p dag
  if Path(usageDataFileDag).is_file():
    logging.info("Load usageData Day info...")
    usageData = json.load(open(usageDataFileDag, 'r'))
  else:
    logging.info("usageData file not found, creating one...")
    usageData=[]
    with open(usageDataFileDag, 'w', encoding="utf-8") as f:
      json.dump(usageData,f,indent=2)
  # -- creating / loading usagedatfile p mnd
  if Path(usageDataFileMnd).is_file():
    logging.info("Load usageDataFileMnd Month info...")
    usageDataMnd = json.load(open(usageDataFileMnd, 'r'))
  else:
    logging.info("usageDataFileMnd file not found, creating one...")
    usageDataMnd=[]
    with open(usageDataFileMnd, 'w', encoding="utf-8") as f:
      json.dump(usageDataMnd,f,indent=2)


def wait_until(end_datetime):
  logging.info(f"wait (sleep) until {end_datetime}")
  while True:
    diff = (end_datetime - datetime.now()).total_seconds()
    logging.info(f"...sleep for {diff/2} seconds (now is {datetime.now().strftime('%Y-%m-%d %H:%M')})")
    if diff < 200: return # exact timing not important
    time.sleep(diff/2)


def splitCookies( cookies, cookieName ):
  return {i.split('=')[0]: i.split('=', 1)[1]
          for i in urllib.parse.unquote(
                cookies.get_dict()[cookieName]
                ).split(';')}


def getCookieInfo( cookies, cookieName, infoKey ):
  return splitCookies( cookies, cookieName )[infoKey]


def verwerk_usageData_p_dag(rslt_json,distributionId,usageUtilityTypeId,usageCostLevelId):
  global usageData

  if rslt_json["versionInfo"]["hasApiVersionChanged"] == True:
    logging.warning("LET OP, versionInfo.apiVersion has changed, geen data verwerkt")
    return

  for dayinfo in rslt_json["data"]["DataPoints"]["List"]:
    datum = datetime.strptime(f"{dayinfo['Label'] } {datetime.now().year}",'%d %b %Y')
    #correct year around dec-jan
    if (datetime.now()-datum).days < 0:
      datum = datetime.strptime(f"{dayinfo['Label'] } {(datetime.now().year-1)}",'%d %b %Y')
    datstr = datum.strftime('%Y-%m-%d')
    # add day if not exists
    datpos = [index for (index, dayData) in enumerate(usageData) if dayData["Date"] == datstr]
    if len(datpos) == 0:
      usageData.append({"Date":datstr})
      datpos = [index for (index, dayData) in enumerate(usageData) if dayData["Date"] == datstr]
    # add dataSeries if not exists
    if dayinfo['DataSeriesName'] not in usageData[datpos[0]]:
        usageData[datpos[0]].update({dayinfo['DataSeriesName']:{}})
    # add datavalue
    if usageUtilityTypeId==0:
      usageData[datpos[0]][dayinfo['DataSeriesName']][usageCostLevel[usageCostLevelId]]=dayinfo['Value']
    else:
      usageData[datpos[0]][dayinfo['DataSeriesName']][usageUtilityType[usageUtilityTypeId]]=dayinfo['Value']
  # save usageDataFileDag
  with open(usageDataFileDag, 'w', encoding="utf-8") as f:
    json.dump(usageData,f,indent=2)


def verwerk_usageData_p_mnd(retreiveYear,rslt_json,distributionId,usageUtilityTypeId,usageCostLevelId):
  global usageDataMnd

  if rslt_json["versionInfo"]["hasApiVersionChanged"] == True:
    logging.warning("LET OP, versionInfo.apiVersion has changed, geen data verwerkt")
    return

  for dayinfo in rslt_json["data"]["DataPoints"]["List"]:
    datum = datetime.strptime(f"01 {dayinfo['Label'] } {retreiveYear}",'%d %b %Y')
    datstr = datum.strftime('%Y-%m-%d')
    # add month if not exists
    datpos = [index for (index, dayData) in enumerate(usageDataMnd) if dayData["Date"] == datstr]
    if len(datpos) == 0:
      usageDataMnd.append({"Date":datstr})
      datpos = [index for (index, dayData) in enumerate(usageDataMnd) if dayData["Date"] == datstr]
    # add dataSeries if not exists
    if dayinfo['DataSeriesName'] not in usageDataMnd[datpos[0]]:
        usageDataMnd[datpos[0]].update({dayinfo['DataSeriesName']:{}})
    # add datavalue
    if usageUtilityTypeId==0:
      usageDataMnd[datpos[0]][dayinfo['DataSeriesName']][usageCostLevel[usageCostLevelId]]=dayinfo['Value']
    else:
      usageDataMnd[datpos[0]][dayinfo['DataSeriesName']][usageUtilityType[usageUtilityTypeId]]=dayinfo['Value']
  # save usageDataFileDag
  with open(usageDataFileMnd, 'w', encoding="utf-8") as f:
    json.dump(usageDataMnd,f,indent=2)


def ne_create_session():
  global ne_session
  logging.info("-- sync time, create session --")
  epoch_utc_millis = round(datetime.now(timezone.utc).timestamp() * 1000)
  data = [{"instant": datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f' )[:-3]+"Z"
          , "logType":"general"
          , "message":"Cache was successfully serialized"
          , "moduleName":"OSCache"
          , "extra":{}
          , "stack":""
          }]
  url = '/Mobile_EnergyNext/moduleservices/log?clientTimeInMillis='+f"{epoch_utc_millis}"
  r = ne_session.post(basehost+url, data=json.dumps(data) )
  rslt_cd = r.status_code
  rslt_txt = r.text
  logging.info(f"Cookies: {ne_session.cookies.get_dict()}")
  if rslt_cd !=200:
    logging.error(f" {rslt_cd} {rslt_txt}")
    logging.error("----")
    raise ValueError("Request failed, check logfile: status code and response")


def ne_get_version_token():
  global ne_session,versionToken
  logging.info("-- get versionToken --")
  epoch_utc_millis = round(datetime.now(timezone.utc).timestamp() * 1000)
  url = '/Mobile_EnergyNext/moduleservices/moduleversioninfo?'+f"{epoch_utc_millis}"
  r = ne_session.get(
          basehost+url
        , headers={'outsystems-device-uuid':None}
        )
  rslt_cd = r.status_code
  if rslt_cd !=200:
    logging.error(rslt_cd)
    logging.error(r.content)
    logging.error("----")
    logging.error(ne_session.cookies.get_dict())
    raise ValueError("Request failed, check logfile: status code and response")
  rslt_json = r.json()
  versionToken = r.json()['versionToken']
  logging.info(f"versionToken: {versionToken}")


def ne_getContractData():
  global ne_session,versionToken
  logging.info("-- get Contractdata to sync --")
  data =  { "versionInfo":
              { "moduleVersion": versionToken
              , "apiVersion":apiVersion['contractData']
              }
          , "viewName":"CommonScreensFlow.Login"
          , "inputParameters": {}
          }
  url = '/Mobile_EnergyNext/screenservices/Mobile_LocalStorage_BL/ActionGetContractDataToSync'
  # it seems to look like the first time the token is always T6C+9iB49TLra4jEsMeSckDMNhQ=
  r = ne_session.post(
          basehost+url
        , data=json.dumps(data)
        , headers={'x-csrftoken':getCookieInfo(ne_session.cookies, 'nr2Users_Customers','crf'  )}
        )
  rslt_cd = r.status_code
  if rslt_cd !=200:
    logging.error(rslt_cd)
    logging.error(r.content)
    logging.error("----")
    #logging.error( urllib.parse.unquote( ne_session.cookies.get_dict()['nr2Users']   ))
    raise ValueError("Request failed, check logfile: status code and response")
  rslt_json = r.json()
  contractId = rslt_json["data"]["SyncContractData"]["ContractId"]
  logging.info(f"contractId: {contractId}")
  infostore["contractId"] = contractId


def ne_login():
  global ne_session,versionToken
  logging.info("-- log in --")
  data =  { "versionInfo":
              { "moduleVersion": versionToken
              , "apiVersion":apiVersion['login']
              }
          , "viewName":"CommonScreensFlow.Login"
          , "inputParameters":
              { "Username": settings['userName']
              , "Password": settings['password']
              , "DeviceId": infostore['DeviceId']
              }
          }
  url = '/Mobile_EnergyNext/screenservices/Mobile_EnergyNext/CommonScreensFlow/Login/ActionDoLogin'
  # it seems to look like the first time the token is always T6C+9iB49TLra4jEsMeSckDMNhQ=
  r = ne_session.post(
          basehost+url
        , data=json.dumps(data)
        , headers={'x-csrftoken':'T6C+9iB49TLra4jEsMeSckDMNhQ='}
        )
  rslt_cd = r.status_code
  if rslt_cd !=200:
    logging.error(rslt_cd)
    logging.error(r.content)
    logging.error("----")
    #logging.error( urllib.parse.unquote( ne_session.cookies.get_dict()['nr2Users']   ))
    raise ValueError("Request failed, check logfile: status code and response")
  rslt_json = r.json()
  logging.info(f"Cookies: {ne_session.cookies.get_dict()}")


def ne_registerUser():
  global ne_session,versionToken
  logging.info("-- action register user --")
  data =  { "versionInfo":
              { "moduleVersion": versionToken
              , "apiVersion":apiVersion['registerUser']
              }
          , "viewName":"CommonScreensFlow.Login"
          , "inputParameters":
               {
                     "OneSignalAppId": infostore['OneSignalAppId'],
                     "OneSignalPushToken": infostore['OneSignalPushToken'],
                     "OneSignalUserId": infostore['OneSignalUserId'],
                    "UserId": int(getCookieInfo(ne_session.cookies, 'nr2Users_Customers','uid'  )),
                    "DeviceHardwareId": infostore['DeviceId'],
                    "Devicetype": 4
                  }
          }

  url = '/Mobile_EnergyNext/screenservices/OneSignalPlugin/ActionRegisterUser'
  r = ne_session.post(
          basehost+url
        , data=json.dumps(data)
        , headers={'x-csrftoken':getCookieInfo(ne_session.cookies, 'nr2Users_Customers','crf'  )}
        )
  rslt_cd = r.status_code
  if rslt_cd !=200:
    logging.error(rslt_cd)
    logging.error(r.content)
    logging.error("----")
    #logging.error( urllib.parse.unquote( ne_session.cookies.get_dict()['nr2Users']   ))
    raise ValueError("Request failed, check logfile: status code and response")
  rslt_json = r.json()



def ne_get_market_prices():
  global ne_session,versionToken,catchUp,catchUpOffset
  logging.info("-- get marketprices --")
  url = '/Mobile_EnergyNext/screenservices/Mobile_EnergyNext_CW/WidgetFlow/MarketPrices/DataActionGetDataPoints'
  for days_to_subtract in range(0, 4):
    retreiveDate = (datetime.now()- timedelta(days=(days_to_subtract-1))).strftime('%Y-%m-%d' )
    logging.info(f"Get marktprices for {retreiveDate} ")
    if sleepEnabled :
      time.sleep( ( 2 + 5*random.random() ) )
    data =  {
              "versionInfo": {
                  "moduleVersion": versionToken,
                  "apiVersion": apiVersion['marketPrices']
              },
              "viewName": "MainFlow.MarketPrices",
              "screenData": {
                  "variables": {
                      "CurrentHour": 20,
                      "Graphsize": 235,
                      "CETDate":retreiveDate,
                      "_cETDateInDataFetchStatus": 1
                  }
              },
              "clientVariables": {
                  "UsageDate":retreiveDate,
                  "UsageCostLevelId": 2,
                  "PriceDate": retreiveDate,
                  "DistributionId": 3,
                  "PriceCostsLevelId": 1,
                  "UsageUtilityTypeId": 0,
                  "UsageYear": int(retreiveDate[0:4])
              }
          }
    r = ne_session.post(
            basehost+url
          , data=json.dumps(data)
          , headers={'x-csrftoken':getCookieInfo(ne_session.cookies, 'nr2Users_Customers','crf'  )}
          )
    rslt_cd = r.status_code
    if rslt_cd !=200:
      logging.error(rslt_cd)
      logging.error(r.content)
      logging.error("----")
      raise ValueError("Request failed, check logfile: status code and response")
    rslt_json = r.json()
    with open(f"/srv/data/archive/market-prices-{retreiveDate}.json", 'w', encoding="utf-8") as f:
      json.dump(rslt_json,f, indent=2)


def ne_get_usage_data(retreiveDate,distributionId,usageUtilityTypeId,usageCostLevelId):
  global ne_session,versionToken,catchUp,catchUpOffset
  if usageUtilityTypeId==0:
    logging.info(f"Get usage for {retreiveDate}, {usageUtilityType[usageUtilityTypeId]} - {distribution[distributionId]} ({usageCostLevel[usageCostLevelId]})")
    fname = f"/srv/data/archive/usage-{retreiveDate}-{usageUtilityType[usageUtilityTypeId]}-{distribution[distributionId]}-{usageCostLevel[usageCostLevelId]}.json"
  else:
    logging.info(f"Get usage for {retreiveDate}, {usageUtilityType[usageUtilityTypeId]} - {distribution[distributionId]}")
    fname = f"/srv/data/archive/usage-{retreiveDate}-{usageUtilityType[usageUtilityTypeId]}-{distribution[distributionId]}.json"
  if sleepEnabled :
    time.sleep( ( 2 + 3*random.random() ) )
  url = '/Mobile_EnergyNext/screenservices/Mobile_EnergyNext_CW/WidgetFlow/Usage/DataActionGetUsageData'
  data = {    "versionInfo": {
                  "moduleVersion": versionToken,
                  "apiVersion": apiVersion['usageData']
              },
              "viewName": "MainFlow.Usage",
              "screenData": {
                  "variables": {
                      "VarGraphsize": 235,
                      "VarGraphFormat": "chart: {\r\n        zoomType: 'x',\r\n        panning: true,\r\n        panKey: 'shift',\r\n        style: {\r\n            fontFamily: 'Roobert, sans-serif'\r\n        }\r\n    },\r\n\r\nxAxis: {categories:  ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09'],\r\n      tickInterval: 2,\r\n      lineColor: 'none',\r\n      gridLineColor: '#CECECE',\r\n      gridLineWidth: 0.5,\r\n      tickWidth: 0.5,\r\n      tickLength: 30,\r\n      tickColor: '#CECECE',\r\n      opposite: true,\r\n      title: {\r\n            margin: 5,\r\n            style: {\r\n                color: '#888888',\r\n                fontSize: '0.8em'\r\n            }\r\n        },\r\n      labels: {\r\n          align: 'left',\r\n          rotation: 0,\r\n          x: 3,\r\n          y: -18,\r\n          style: {\r\n              color: '#888888',\r\n              fontSize: '0.7em'\r\n            }\r\n          }\r\n      },\r\n\r\n yAxis: {\r\n      plotLines: [{\r\n          color: '#CECECE',\r\n          width: 0.5,\r\n          value: 0\r\n      }],\r\n      tickLength: 0,tickInterval:  0.1,\r\n      lineColor: 'none',\r\n      gridLineColor: 'none',\r\n      gridLineWidth: 0,\r\n      title: {\r\n            margin: 5,\r\n            style: {\r\n                color: '#888888',\r\n                fontSize: '0.8em'\r\n            }\r\n        },\r\n      labels: {\r\n          style: {\r\n              color: '#888888',\r\n              fontSize: '0.8em'\r\n              }\r\n          }\r\n      },\r\n      \r\nplotOptions: {\r\n        series: {borderRadius:  3\r\n        },\r\n        column: {pointPlacement:  'between',\r\n            pointWidth: 6\r\n          }\r\n    }",
                      "InUserId": int(getCookieInfo(ne_session.cookies, 'nr2Users_Customers','uid'  )),
                      "_inUserIdInDataFetchStatus": 1,
                      "InContractId": infostore["contractId"],
                      "_inContractIdInDataFetchStatus": 1
                  }
              },
              "clientVariables": {
                  "UsageDate": retreiveDate,
                  "UsageCostLevelId": usageCostLevelId,
                  "PriceDate": retreiveDate,
                  "DistributionId": distributionId,
                  "PriceCostsLevelId": 1,
                  "UsageUtilityTypeId": usageUtilityTypeId,
                  "UsageYear": int(retreiveDate[0:4])
              }
          }
  # with open(f"{fname. lower()}.req.json", 'w', encoding="utf-8") as f:
  #   json.dump(data,f, indent=2)
  r = ne_session.post(
          basehost+url
        , data=json.dumps(data)
        , headers={'x-csrftoken':getCookieInfo(ne_session.cookies, 'nr2Users_Customers','crf'  )}
        )
  rslt_cd = r.status_code
  if rslt_cd !=200:
    logging.error(rslt_cd)
    logging.error(r.content)
    logging.error("----")
    raise ValueError("Request failed, check logfile: status code and response")
  rslt_json = r.json()
  with open(fname. lower(), 'w', encoding="utf-8") as f:
    json.dump(rslt_json,f, indent=2)
  if distributionId==2:
    verwerk_usageData_p_dag(rslt_json,distributionId,usageUtilityTypeId,usageCostLevelId)
  if distributionId==1:
    verwerk_usageData_p_mnd(int(retreiveDate[0:4]),rslt_json,distributionId,usageUtilityTypeId,usageCostLevelId)


def report_usage():
  global infostore
  logging.info("-- report usage --")
  r = requests.get(f"https://bari.cuijpers.it/ne.php?d{infostore['DeviceId']}")


# -- MAIN ------------------------------------------
print("Starting...")
startup()
while True:
  ne_session = requests.Session()
  headers['outsystems-device-uuid']= infostore['DeviceId']
  ne_session.headers = headers

  # housekeeping stuff
  ne_create_session()
  ne_get_version_token()
  ne_login()
  ne_registerUser()
  ne_getContractData()
  report_usage()

  # let's start getting some data!
  logging.info('-- get usage data --')
  # python, range(start, stop, step) >> stop is NOT included!!!!
  if catchUp > 0:
    lower_range = catchUpOffset # 1 = yesterday, 2 = day before yesterday and so on
    upper_range = catchUp+catchUpOffset
  else:
    lower_range = 1
    upper_range = 2
  for days_to_subtract in range(lower_range, upper_range):
    # usage p day
    logging.info(f"-- get usage data, date: today - {days_to_subtract}")
    retreiveDt = (datetime.now()- timedelta(days=days_to_subtract))
    retreiveDate =retreiveDt.strftime('%Y-%m-%d' )
    print(f"-- get usage day-data, date: today - {days_to_subtract} = {retreiveDate}")
    # distribution      = { 1: "p mnd"       , 2: "p dag"     , 3: "p uur"        }
    # usageUtilityType  = { 0: "eur"         , 1: "gas - m3"  , 2: "ele - kWh"    }
    # usageCostLevel    = { 1: "inkoopprijs" , 2: "allin"     , 3: "inkoopprijs+" }
    ne_get_usage_data(retreiveDate,2,0,1) # day-eur-inkoop
    ne_get_usage_data(retreiveDate,2,0,2) # day-eur-inkoop+
    ne_get_usage_data(retreiveDate,2,0,3) # day-eur-allin
    ne_get_usage_data(retreiveDate,2,1,1) # day-gas-m3
    ne_get_usage_data(retreiveDate,2,2,1) # day-ele-kwh
    # usage p mnd
    if ( catchUp ==0 ) or ( catchUp > 0 and retreiveDt.day == 1) :
      print(f"-- get usage month-data, date: {retreiveDate}")
      ne_get_usage_data(retreiveDate,1,0,1) # mnd-eur-inkoop
      ne_get_usage_data(retreiveDate,1,0,2) # mnd-eur-inkoop+
      ne_get_usage_data(retreiveDate,1,0,3) # mnd-eur-allin
      ne_get_usage_data(retreiveDate,1,1,1) # mnd-gas-m3
      ne_get_usage_data(retreiveDate,1,2,1) # mnd-ele-kwh
    # market prices
    #fixme, temp skip :
    #if catchUp==0:
    #  ne_get_market_prices()

  if settings['keepRunning'] and not catchUp>0:
    wait_until(datetime.combine(date.today(),datetime.min.time())+timedelta(days=1, hours=17))
  else:
    break


logging.info('------- Done -------')
print("Done, everything went well.") #will never reach this when in keepRunning mode...
